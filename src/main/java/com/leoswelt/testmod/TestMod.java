package com.leoswelt.testmod;

import net.minecraft.entity.passive.EntityVillager.EmeraldForItems;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = "testmod", name = "TestMod", version = "1.0")
public class TestMod {

	@Instance
	public static TestMod instance = new TestMod();
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		System.out.println("preInit-Methode lädt.");
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e) {
		loadCraftingRecipes();
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		System.out.println("postInit-Methode lädt.");
	}
	
	public void loadCraftingRecipes() {
		GameRegistry.addRecipe(new ItemStack(Items.COOKIE, 55), new Object[] {
				"###",
				"#U#",
				"###",
				'#', Items.COOKIE,
				'U', Items.GOLDEN_CARROT
		});
		
		GameRegistry.addShapelessRecipe( new ItemStack(Items.TOTEM, 2), new Object[] { Items.GOLD_INGOT, Items.EMERALD } );
		
		GameRegistry.addSmelting(Items.SADDLE, new ItemStack(Items. DARK_OAK_DOOR), 1.18F);
		
		GameRegistry.addRecipe(new ItemStack(Items. COOKED_CHICKEN, 64), new Object[] {
				"123",
				"456",
				"789",
				'1', Items.BEEF,
				'2', Items.GHAST_TEAR,
				'3', Items.GOLDEN_APPLE,
				'4', Items.COMPASS,
				'5', Items.BED,
				'6', Items.ELYTRA,
				'7', Items.ENCHANTED_BOOK,
				'8', Items.WHEAT,
				'9', Items.ROTTEN_FLESH
		
		});
	}
}
		
	
		