package com.leoswelt.testmod;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;

public class SuperBlock extends Block	{

	public SuperBlock(Material material, String unlocalizedName, float hardness, float resistance, float lightLevel,  String tool, int harvestLevel ) {
		super(material);
		this.setUnlocalizedName(unlocalizedName);	
		this.setHardness(hardness);
		this.setResistance(resistance);
		this.setLightLevel(lightLevel);
		this.setHarvestLevel(tool, harvestLevel );
	
		this.registerTexture(unlocalizedName);
	}
	
	private void registerTexture(String unlocName) {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
			.register(Item.getItemFromBlock(this), 0, new ModelResourceLocation("testmod:" + unlocName));
	}
	
}
	

